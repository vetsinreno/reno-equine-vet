**Reno equine vet**

Our equine veterinarian in Reno is a full-service equine veterinarian who provides horses in Northern Nevada 
with advanced veterinary care. Our service area includes the greater Reno district, 
Sparks, Spanish Springs, the North Valleys, Washoe Valley, Carson City and Carson Valley. 
With our sister practice, Great Basin Equine, located in Gardnerville, we serve as equine medicine and
surgical referral hospitals for Northern Nevada and Northern California.
Please Visit Our Website [Reno equine vet](https://vetsinreno.com/equine-vet.php) for more information.

---

## Our equine vet in Reno  services

Our mission in Reno is to provide advanced equine veterinary service with the highest value to our customers and 
to ensure that our patients receive the highest level of comfort and care. 
Founded in 1971, our practice has evolved and expanded into a predominantly equine, multi-doctor 
ambulatory and surgical facility as a one-doctor practice serving cattle and horse ranches.
Our Reno Equine Vet Our experienced veterinarians and staff offer a variety of services, including a 
24-hour on-call emergency service.
